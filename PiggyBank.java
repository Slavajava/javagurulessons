package lv.javaguru.homework;

import java.util.Random;

public class PiggyBank {
    public static void main(String[] args) {

        Random numberGenerator = new Random();

        int oneCentCoins = numberGenerator.nextInt(101);
        int twoCentCoins = numberGenerator.nextInt(101);
        int fiveCentCoins = numberGenerator.nextInt(101);
        int tenCentCoins = numberGenerator.nextInt(101);
        int twentyCentCoins = numberGenerator.nextInt(101);
        int fiftyCentCoins = numberGenerator.nextInt(101);
        int fakeCoins = numberGenerator.nextInt(101);

        int amountOfCoins = oneCentCoins + twoCentCoins + fiveCentCoins + tenCentCoins + twentyCentCoins + fiftyCentCoins + fakeCoins;
        int amountOfCentCoins = amountOfCoins - fakeCoins;

        int sumOneCents = oneCentCoins;
        int sumTwoCents = twoCentCoins * 2;
        int sumFiveCents = fiveCentCoins * 5;
        int sumTenCents = tenCentCoins * 10;
        int sumTwentyCents = twentyCentCoins * 20;
        int sumFiftyCents = fiftyCentCoins * 50;

        int eurInPiggyBank = (sumOneCents + sumTwoCents + sumFiveCents + sumTenCents + sumTwentyCents + sumFiftyCents) / 100;
        int centsInPiggyBank = (sumOneCents + sumTwoCents + sumFiveCents + sumTenCents + sumTwentyCents + sumFiftyCents) % 100;


        System.out.println("Your Piggy bank contains " + amountOfCoins + " coins.");
        System.out.println("Among them " + amountOfCentCoins + " - are real cents coins, and " + fakeCoins + " fake, or different coins.");
        System.out.println("_________________________________________________________________________________");
        System.out.println("| Coins    |  1 cent  |  2 cent  |  5 cent  |  10 cent  |  20 cent  |  50 cent  |");
        System.out.println("---------------------------------------------------------------------------------");
        System.out.println("  Pieces         " + oneCentCoins + "         " + twoCentCoins + "         " + fiveCentCoins + "         " + tenCentCoins + "          " + twentyCentCoins + "          " + fiftyCentCoins + "   ");
        System.out.println("---------------------------------------------------------------------------------");
        System.out.println("  Value          " + sumOneCents + "         " + sumTwoCents + "        " + sumFiveCents + "        " + sumTenCents + "         " + sumTwentyCents + "        " + sumFiftyCents + "   ");
        System.out.println("---------------------------------------------------------------------------------");
        System.out.println("  Total Piggy deposit:                            EUR:   " + eurInPiggyBank + "   |  CENTS:   " + centsInPiggyBank);
        System.out.println("_________________________________________________________________________________");

    }
}
