package lv.javaguru.homework;

import java.util.Random;

public class AnyTriangleSquare {
    public static void main(String[] args) {
        int aSide;
        int bSide;
        int cSide;

        final String ANSI_RESET = "\u001B[0m";
        final String ANSI_RED = "\u001B[31m";
        final String ANSI_GREEN = "\u001B[32m";
        final String ANSI_YELLOW = "\u001B[33m";
        final String ANSI_CYAN = "\u001B[36m";
        final String ANSI_PURPLE = "\u001B[35m";

        Random numberGenerator = new Random();

        do {
            aSide = numberGenerator.nextInt(101);
            bSide = numberGenerator.nextInt(101);
            cSide = numberGenerator.nextInt(101);
        }
        while (aSide + bSide <= cSide || bSide + cSide <= aSide || aSide + cSide <= bSide);

        double halfPerimeter = (aSide + bSide + (double) cSide) / 2;

        double square = Math.sqrt(halfPerimeter * (halfPerimeter - aSide) * (halfPerimeter - bSide) * (halfPerimeter - cSide));

        System.out.println(ANSI_YELLOW + "Random triangle figure with it side's values: " + ANSI_RED +
                +aSide + ANSI_YELLOW + " , " + ANSI_GREEN + bSide + ANSI_YELLOW + " , "
                + ANSI_CYAN + cSide + ANSI_YELLOW + " has a square "
                + ANSI_PURPLE + square + ANSI_RESET);

    }
}
